FROM alpine:latest

RUN apk add --no-cache transmission-daemon

CMD transmission-daemon -f -g /config
